# Testing to Stable

## Setup

* [ ] Add example.com to /etc/hosts.
* [ ] Start with a clean machine running the relevant version of Debian.

## Install

* [ ] Run Install script as per https://wiki.sympl.io/Installation_Instructions without dpkg prompts.
* [ ] User is pointed to https://wiki.sympl.io for docs, and https://forum.sympl.io for issues.
* [ ] User has to set a new password for `sympl`, and is suggested to use an SSH key.
* [ ] User can log in as the `sympl` user.

## Core

* [ ] Banner happens on login and provides correct version/system stats.
* [ ] Typical utilities such as vim, htop, etc are installed and work normally.

## Web

* [ ] `mkdir -p /srv/example.com/public/htdocs`, make sure you are served a 'theres nothing here yet' page.
* [ ] `echo 'Testing example.com' > /srv/example.com/public/htdocs/index.html`, check the page loads with the new content.
* [ ] `echo '<?php phpinfo() ?>' > /srv/example.com/public/htdocs/index.php`, check the page loads with phpinfo.
* [ ] `sudo sympl-web-configure --verbose`, check /srv/example.com/ contains public/logs, php_tmp, php_sessions.
* [ ] Browse to http://example.com again, check logs are being written to `public/logs/access.log`.
* [ ] Browse to https://example.com again (expect browser warning), check logs are being written to `public/logs/ssl_access.log`.
* [ ] `sudo sympl-web-rotate-logs`, check logs have rotated.
* [ ] `sudo sympl-web-generate-stats --verbose`, check stats have NOT been created.
* [ ] `mkdir -p /srv/example.com/config ; echo selfsigned > /srv/example.com/config/ssl-provider ; sudo sympl-ssl --verbose`, check cert is generated.
* [ ] `sudo sympl-web-configure --verbose`, check site now loads with self-signed certificate.

## FTP

* [ ] Confirm you cannot login anonymously via FTP.
* [ ] `echo some-password > /srv/example.com/config/ftp-password`, check you can log in with user `example.com` password `some-password` via FTP and are placed in public.
* [ ] Confirm you can upload/download/delete files via FTP.
* [ ] `echo someuser:someotherpass:htdocs:0M > /srv/example.com/config/ftp-users`, check you can log in with user `someuser@example.com` password `someotherpass` via FTP and are placed in htdocs.
* [ ] Confirm you can download but not upload files via FTP.
* [ ] `sudo sympl-password-test --verbose`, confirm password warning.

## Mail & WebMail

* [ ] `mkdir -p /srv/example.com/mailboxes/user ; echo some-password > /srv/example.com/mailboxes/user/password ; sudo sympl-password-test --verbose`, confirm password warning.
* [ ] Browse to https://example.com/webmail, log in with `user` and `password`
* [ ] `echo new-password > /srv/example.com/mailboxes/user/password`, log out of webmail.
* [ ] Confirm you cannot log in with old password.
* [ ] Confirm you can log in with new password.
* [ ] `sudo sympl-mail-encrypt-passwords --verbose` 
* [ ] Log out and back in again.
* [ ] Send mail to a gmail address, confirm bounce/delivery.
* [ ] `openssl genrsa -out /srv/example.com/config/dkim.key 2048 ; chmod 640 /srv/example.com/config/dkim.key ; chown admin:Debian-exim /srv/example.com/config/dkim.key ; touch /srv/example.com/config/dkim`
* [ ] Send email again, check for DKIM record in bounce/delivery.

## Network

* [ ] `ip a ; sympl-ip`, confirm IPs match.
* [ ] `echo 10.111.234.56 > /srv/example.com/config/ip ; sudo sympl-configure-ips --verbose`, confirm new IP picked up.
* [ ] `ip a ; sympl-ip`, confirm '10.111.234.56' now listed on both results.
* [ ] `sudo iptables -L -n | grep -c ':1234'`, confirm result is 0.
* [ ] `touch /etc/sympl/firewall/incoming.d/99-1234 ; sudo sympl-firewall`
* [ ] `sudo iptables -L -n | grep -c ':1234'`, confirm result is 2.
* [ ] `touch '/etc/sympl/firewall/blacklist.d/10.9.8.7|31' ; sudo sympl-firewall`
* [ ] `sudo iptables -L -n | grep -c '10.9.8.6'`, confirm result is 1.

## MySQL / MariaDB & phpMyAdmin

* [ ] `mysql -e 'show databases'`, confirm databases are listed.
* [ ] Browse to http://example.com/phpmyadmin, confirm redirected to HTTPS.
* [ ] `cat ~/mysql_password`, log in with user `sympl` and password.
* [ ] Confirm no errors/warnings, database can be created.

## Monit

* [ ] `sudo service apache2 stop ; sudo service apache2 status ; sudo sympl-monit ; sudo service apache2 status ;`, confirm apache is started again.


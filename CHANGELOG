CHANGELOG
---------

2024-07-08
  sympl-firewall
    * Persist firewall configuration on boot

2024-05-07
  sympl-core
    * Fix initial sympl-ssl set selection. Thanks to @techwilk for the report and fix.

2024-04-26
  sympl-web
    * Improve PHP selection with sympl-php-wrapper

2024-04-22
  sympl-mail
    * Allow testing mail sending locally
    * Replace internal Antivirus testing
    * Fix mis-named monit link
  sympl-web
    * Improve notice regarding editing configurations
    * Webmail directive fixes
  sympl-mysql
    * Update Monit script to MariaDB

2024-04-19
  sympl-core
    * Added 'sympl version' to output a list of package versions
  sympl-web
    * Fixed Webmail access when running a site with PHP-FPM

2024-03-22
  sympl-web
    * Added new sympl-php-wrapper which runs the relevant version of PHP based on where you execute it it from
      Huge thanks to Alex of Mythic Beasts who did 99% of the work on this!

2024-03-19
  sympl-core
    * Updated sympl-filesystem-security for better composer compatibility
    * Added dbus dependency

2024-03-15
  sympl-web
    * Updated Apache templates with new Mozilla recommendations
    * Fix issue with open_basedir not being unset with FPM

2024-03-08
  sympl-web
    * sympl-php-configure: Update Sury PHP repo process
    * sympl-php-configure: Ensure directories are created
    * sympl-php-configure: Handle edge cases with partially installed PHP packages

2023-11-03
  sympl-web
    * Improve sympl-web-logger version detection logic
    * Enable apache2 sympl-web.conf

2023-11-01
  sympl-web, sympl-firewall, sympl-cron
    * Sympl is now architecture independent, and can be run on any hardware running Debian

2023-06-14
  sympl-web
    * Updated ssl template ssl-only configuration
    * Adjustment to parsing PHP modules with apt search

2023-06-09
  all
    * Debian Bookworm release
    * Various small adjustments to bring things in line with the new Debian release
  sympl-core
    * Automatically populate /etc/hosts on install
    * Include git package as as dependency for future functionality
    * sympl-filesystem-security - Don't change permissions on sympl/php or sympl/monit.d
    * Reset ssl-provider default domain if it was changed to 'selfsigned'
    * Added basic usage reporting. Touch /etc/sympl/disable-usage-reporting to disable
  sympl-web
    * Added sympl-php-configure, allowing fine-grained control over PHP configurations
    * sympl-php-configure runs hourly, and then runs sympl-web-configure
    * sympl-web-configure no longer runs hourly itself, but can still be run manually
    * New config file: php - selects a particular version of PHP for the site
    * New config file: php-pool - allow you to have a seperate pool for the site, sharable with other sites
    * New config file: php-modules - allows providing a list of extra modules/extensions needed
    * New config file: php-user - select a user to run PHP as, defaults to www-data
    * New config file: php-group - select a group to run PHP as, defaults to www-data
    * New config file: php-fpm - denotes that the domain should be configured under PHP-FPM
    * Changes can be made to the PHP Pool configuration with includes
    * Default to blocking all hidden files and directories other than /.well-known/
    * Added allow-hidden switch to allow hidden files via the web
    * Automatically generate .user.ini files from entries in .htaccess where needed
    * Significant update for Apache template files
    * Enable modules cleanly on install
  sympl-backup
    * Automatically purge the oldest backup set if you are out of space and have mutiple sets
    * Automatically remove orphaned database dumps in /var/backups/mysql older than 90 days
  sympl-mail
    * Cleanup of mailbox rate limit processing from @alphacabbage1

2022-11-24
  sympl-web
    * Enable SSLHonorCipherOrder

2022-09-29
  sympl-mail
    * Correctly parse quota files when present
    * Drastically reduce antivirus resource requirments

2022-07-19
  sympl-firewall
    * Reduce automatic whitelist/blacklist matching

2022-07-11
  sympl-firewall
    * Fixed critical bug in DNS name handling

2022-04-26
  sympl-core
    * Fix control logic in sympl-filesystem-security

2022-04-06
  sympl-web
    * Use AWFFull names for stats files rather than Webalizer

2022-03-23
  sympl-core
    * Replace tempfile with mktemp in sympl-generate-dhparams
  sympl-mail
    * Correctly regenerate dhparams for dovecot

2022-03-22
  sympl-mail
    * Clean up logging warning in exim main.log
    * Update references from sympl.host to sympl.io
  sympl-webmail
    * Fix log warnings from Roundcube in user.log
  sympl-core, sympl-web, sympl-cron
    * Update references from sympl.host to sympl.io

2021-12-13
  sympl-core
    * Updated workaround for Let's Encrypt cross-signed intermediate
    * Fix for MOTD installation

2021-10-03
  sympl-core
    * Workaround for Let's Encrypt cross-signed intermediate

2021-10-01
  sympl-core
    * Updated acme-client library to 2.0.9

2021-09-21
  sympl-mail
    * Deal with aliases correctly when no mailboxes directory exists

2021-08-19
  all packages
    * Debian Bullseye Release
  sympl-core
    * Updates to MOTD and banners

2021-08-18
  sympl-core
    * Check htdocs/stats for AWFFull rather than Webalizer
  sympl-web
    * Update Webalizer references to AWFFull
    * Disable default AWFFull cron
  sympl-mail
    * Further fixes for Exim 4.94 in Debian Bullseye
  
2021-08-13
  sympl-mail
    * use systemd socket activation for sympl-mail-poppassd    

2021-04-09
  sympl-core
    * Update sympl.host to sympl.io

2021-04-08
  sympl-mail
    * Fix tag option for antispam and antivirus configs (#310)
    * Mail identified as spam now adds '[spam]' to the subject of incoming mail

2021-02-15
  all packages
    * Updated version numbering format
  sympl-mysql
    * Fixed creating 'sympl' MySQL user
  sympl-web
    * Removed deprecated Apache VHost mod

2021-02-12
  sympl-mail
    * Final fixes for Exim 4.94

2021-02-11
  sympl-mail
    * Further fixes for Exim 4.94

2021-02-10
  sympl-mail
    * Fixes for Exim 4.94

2021-02-09
  all packages
    * Updated versioning for Debian Bullseye
    * Tidied up old dependencies
  sympl-web
    * Replaced Webalizer with AWFFull
    * Removed deprecated php-mcrypt
  sympl-core
    * Updated MOTD version number
    
2020-09-23
  sympl-core
    * Properly filter public/cgi-bin

2020-09-15
  sympl-phpmyadmin
    * Package now available in Buster, using phpMyAdmin from Debian Backports
  sympl-core
    * Enabled Debian Backports repo for Buster to allow installation of phpMyAdmin

2020-09-09
  sympl-web
    * Adds support for optional Apache configs in config/apache.d/*.conf (#300)
    * Added php-zip package to recommends (#294)
  sympl-core
    * sympl-filesystem-security: don't overwite permission in public/cgi-bin (#299)
    * sympl-filesystem-security: correctly read the group id (#298)
    * sympl-cli: fix permissions on newly created domains (#295)

2020-07-06
  sympl-web
   * Fixes incorrect filename for log files (#296)
  sympl-backup
   * Adds missing -extract function to DRIVER_TAR_GZ (#297)

2020-05-12
  sympl-core
    * Added functionality to the sympl cli for managing FTP users

2020-05-10
  sympl-core
    * Remove debug output from sympl-filesystem-security

2020-04-27
  sympl-core
    * Further fixes to prevent sympl-filesystem-security from changing permissions where it shouldn't. (#280) 

2020-04-22
  sympl-web
    * Switch to individual packages for sympl-web (#292)
    * Only enable OCSP Stapling for certs that support it (#293)

2020-04-20
  sympl-core
    * Prevent sympl-filesystem-security from changing permissions of /etc/firewall/local.d/ contents.

2020-04-18
  sympl-mail
    * Fixed sympl-mail-dovecot-sni issue with filesystem loops (#281)

2020-04-15
  sympl-core
    * Added --verbose switch to sympl-filesystem-security
    * Fixed issue #280 with sympl-filesystem-security

2020-03-26
  sympl-monit
    * Don't use sudo when writing cursor. Fixes issue #279.
    * Update sympl-monit.cursor path.

2020-01-27
  sympl-webmail
    * Fixed importing contacts

2019-12-31
  sympl-core
    * Fixed inconsistency with 'disable-filesystem-security' switch.

2019-12-27
  sympl-mail
    * Improves default PCI Compliance by disabling TLS1.0
    * Fixes dhparam issue with Dovecot

2019-12-16
  sympl-core
    * Add sympl user to relevant groups on each install.
  sympl-web
    * Added cron to clean up old PHP sessions.

2019-12-05
  sympl-core
    * Updated IPv6 Only workaround for sympl-ssl.

2019-10-17
  sympl-core
    * Updated sympl-ssl to use Let's Encrypt ACME v02 API.

2019-10-04
  sympl-mail
    * Fixed permission issue with configuration.

2019-09-18
  sympl-firewall
    * Fixed missing version increment.

2019-09-17
  sympl-mail
    * Adds full chain support to Dovecot SNI, needed by some clients.

2019-09-08
  sympl-core
    * Set default threshold for LE cert renewal to suggested 30 days.
  sympl-backup
    * Added backup2l driver to prevent warnings from tar.

2019-08-16
  sympl-core
    * Adds detection of NAT64 environments for sympl-ssl wrapper.
  sympl-firewall
    * Removed incrond, re-instated old manual triggers on changes.
    * Fixed warning message from nftables.

2019-07-31
  sympl-backup
    * Force backups to be run as root.
    * Updated backup paths, exclude backups of /var/lib/docker.
  sympl-mysql
    * Updated sympl-sqldump to use sympl user correctly.

2019-07-29
  sympl-core
    * Copy root user authorized_keys to sympl user on first install.

2019-07-25
  sympl-core
    * Fixed typo in sympl CLI

2019-07-19
  sympl-cron
    * Updated sympl-crontab --test output

2019-07-18
  sympl-firewall
    * Updated sympl-firewall-whitelist to more sane defaults.
    * Only whitelist SSH access for a week once logged in.
    * Only whitelist IPv6 address at /128 rather than /64.

2019-07-09
  sympl-web
    * Updated sympl-web-rotate-logs to support new ownership
    * Reload Apache when rotating logs, rather than the loggers.

2019-07-08
  sympl-mail
    * Re-enable Dovecot SNI

2019-07-07
  sympl-mail
    * Fixed unhandled input

2019-07-06
  sympl-mail
    * Resolved potential race condition
    * Updated sympl-mail-dovecot-sni for edge cases
    * Improved sympl-mail ssl-hook

2019-07-05
  sympl-core
    * Removed beta flag from MOTD
    * Updated 'sympl' parser, added 'sympl update' function.

2019-07-04
  sympl-core
    * Workaround for sympl-ssl bug #249 under IPv6 only.
  sympl-monit
    * Updated monit tests to use TLSv1.2
  sympl-web
    * Rewrote Apache configs
    * Moved phpMyAdmin specifics to sympl-phpmyadmin
    * Deprecated Apache vhost_sympl module

2019-07-03
  sympl-web
    * Updated path for PHP config
    * Reverted default PHP lockdown
    * Reverted vhost rewrites

2019-07-02
  sympl-core
    * Removed mailbox permission rewriting
    * Disabled hostname enforcement
    * Adjusted security permissions for domains Exim config files
  sympl-mail
    * Adjusted exim config group
    * Permissions adjustment for Debian-exim user

2019-07-01
  sympl-mail
    * Fixes for Roundcube/Dovecot changes in Buster.
    * Enables SMTP AUTH on localhost without TLS.

2019-06-30
  sympl-web
    * Reworked apache templates
    * Added fallback for zz-mass-hosting

2019-06-28
  sympl-core
    * Adjusted permissions for config/dkim

2019-06-26
  sympl-mail
    * Fix for non-selfsigned certs with Dovecot SNI
    * Fixed SNI configuration in Exim and Dovecot

2019-06-25
  sympl-core
    * First update for sympl command line
    * Fixed edge case in sympl-filesystem-security

2019-06-24
  sympl-core
    * Adjusted MOTD Banner
    * Updated sympl-filesystem-security with tweaks to paths/logic
  sympl-ftp
    * Adjusted configuration to allow www-data
  sympl-mail
    * Updated Dovecot configuration for Debian Buster
    * Migrated links into existing file
  sympl-web
    * Adjusted ssl-hook so it doesn't fire before sympl-web is installed.
    * Adjusted Apache templates slightly.

2019-06-21
  All Packages
    * Created Sympl v10.0 (Debian Buster)
  sympl-core
    * Moved sympl-ssl to sbin to avoid permissions/hook issues.
  sympl-web
    * Updated dependencies/build-dependencies
    * Fixed typo in apache template

2019-06-20
  sympl-mail
    * Merged legacy Symbiosis patch for SNI on Exim
    * Updated configuration for SNI in Dovecot
  sympl-core
    * Updated recommended packages
    * Updated MOTD banner

2019-06-19
  sympl-web
    * Massively improved security for PHP
    * PHP is now restricted to public/, and has domain-specific tmp and
      sessions directories which are automatically created.
    * PHP is now disabled in a path that matches 'wp-content/uploads'
      significantly securing all WordPress sites.
    * Enables OSCP stapling by default. Disables HSTS by default.
    * zz-mass-hosting now configures all sites, not just SSL sites.
    * sympl-web-logger now only used for the zz-mass-hosting fallbacks.
    * PHP can block dangerous functions such as eval() and exec() which
      should not be needed typically. This can be enabled manually
      but effects all sites on the server.
    * new config files: config/disable-php-security and config/hsts.
  sympl-webmail
    * Updated configuration to restrict PHP directory access

2019-06-14
  sympl-mysql
    * Fixed typos

2019-06-13
  sympl-backup
    * Removed deprecated backup scripts
  sympl-mysql
    * Added sympl-sqldump

2019-06-12
  sympl-web
    * Massively improved security for web stats.
    * new config files: config/stats and config/stats-htpasswd
  sympl-webmail
    * Improved webmail auto-configuration

2019-06-11
  All Packages
    * Merged sympl-common into sympl-core

2019-06-10
  All Packages
    * Adjusted Dependencies
  sympl-mail
    * Re-implimented password strength testing
  sympl-webmail
    * Moved configuration of roundcube to sympl-common

2019-06-09
  All Packages
    * Renamed admin user to sympl.
  sympl-ftp
    * FTP user now logs in as owner of the chrooted dir
    * Added Umask so files are +rw by the relevant group
  sympl-web
    * Removed skel.d files.
  sympl-mysql
    * .my.cnf and 'mysql_password' files now created in /home/sympl
  sympl-backup
    * Updated backup paths

* 2019-06-06 - First Public Build
  - Renamed packages and files, replaced references to Symbiosis with Sympl.
      bytemark-symbiosis -> sympl-core
      symbiosis-httpd -> sympl-web
      symbiosis-email -> sympl-mail
      symbiosis-ftpd -> sympl-ftp
      symbiosis-meta -> sympl-core
      symbiosis-* -> sympl-*
  - Renamed command-line tools, with new package names, added symlinks
    from old names.
  - /etc/symbiosis is now /etc/sympl, with symlink for compatibilty.
  - Folded old metapackages into relevant packages.
  - Dropped support for old Exchange Activesync.
  - Dropped support for XMPP.
  - A lot of tidying up.

* 2019-05-28 - Implemented autotest suite, updated docs.

* 2019-04-16 - Fixed Gitlab CI

* 2019-04-13 - Initial fork from GitHub

#!/bin/bash -e

# Workaround wrapper script for sympl-ssl to deal with bugs:
# 1. in IPv6 only resolution of the LE API DNS
# 2. with extra expired LE intermediates which sympl-ssl considers invalid

exit_code=0

# If theres no IPv4 address assigned...
if [ $( sympl-ip -a | grep -c '\.' ) == 0 ] || [ $( getent hosts ipv4only.arpa | grep -c ':' ) != 0 ] ; then
  if [[ $@ == *'--verbose'* ]]; then echo 'Applying IPv6 only workaround...'; fi

  # Do a DNS lookup for acme-v02.api.letsencrypt.org...
  ipv6=$( dig -t AAAA acme-v02.api.letsencrypt.org +short | grep ':' | head -n 1 )
  # ... and add it to /etc/hosts
  echo -e "$ipv6\facme-v02.api.letsencrypt.org # sympl-ssl workaround" >> /etc/hosts
  # run sympl-ssl with all the parameters passed
  # stop exiting on errors, and store the result for the end
  set +e
  /usr/sbin/sympl-ssl.rb $@
  exit_code="$?"
  # re-enable exiting on errors
  set -e
  # and then remove the line from /etc/hosts
  sed -i -n '/# sympl-ssl workaround/!p' /etc/hosts

  if [[ $@ == *'--verbose'* ]]; then echo 'Removed IPv6 only workaround'; fi
else
  # stop exiting on errors, and store the result for the end
  set +e
  /usr/sbin/sympl-ssl.rb $@
  exit_code="$?"
  # re-enable exiting on errors
  set -e
fi

find /srv/*/config/ssl/sets/ \( -name 'ssl.bundle' -o -name 'ssl.combined' \) -exec grep -lx '^MIIFYDCCBEigAwIBAgIQQAF3ITfU6UK47naqPGQKtzANBgkqhkiG9w0BAQsFADA/$' {} \; | while read file ; do
    input="$( cat "$file" | tr '\n' '\t' )"
    echo -e "$input" \
    | sed 's|\tnLRbwHOoq7hHwg==\t-----END CERTIFICATE-----\t-----BEGIN CERTIFICATE-----\tMIIFYDCCBEigAwIBAgIQQAF3ITfU6UK47naqPGQKtzANBgkqhkiG9w0BAQsFADA/\t.*\tDfvp7OOGAN6dEOM4+qR9sdjoSYKEBpsr6GtPAQw4dy753ec5\t-----END CERTIFICATE-----|\tnLRbwHOoq7hHwg==\t-----END CERTIFICATE-----|' \
    | tr '\t' '\n' > "$file"
done

exit $exit_code

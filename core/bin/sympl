#!/bin/bash
#
# Sympl command line interface.
#
# Copyright 2019-2024 the Sympl Project - https://sympl.io
#
# Licenced under GPL3+
#
set -e

I_AM="$(whoami)"
VERBOSE=TRUE
FUNCTION=base
ACTION=none
TARGET=all

##############################################################################
#                              Output Functions                              #
##############################################################################

_debug() {
  if [ $DEBUG ]; then
    echo -e "\033[2mDEBUG: $@\033[0m"
  fi
}

_warn() {
  echo -e "\033[1m\033[33m WARN: \033[0m $@"
}

_error() {
  echo -e "\033[1m\033[31mERROR: \033[0m $@"
  exit 1
}

_verbose() {
  if [ $DEBUG ]; then
    echo " INFO: $@"
  elif [ $VERBOSE ]; then
    echo "$@"
  fi
}

##############################################################################
#                                    Help                                    #
##############################################################################

_help() {
  echo 'This is a big set of parser stuff.'
  exit 0
}

##############################################################################
#                              Support Functions                            #
##############################################################################

_func() {
  if [ $FUNCTION == base ]; then
    FUNCTION=$1
  else
    _error "Function '$FUNCTION' already selected, can't set to $1"
  fi
}

_action() {
  if [ $ACTION == none ]; then
    ACTION=$1
    if [ "$ACTION" == "set" ] && [ "x$3" != "x" ]; then
      OPTION=$2
      SETTING=$3
    fi
  else
    _error "Action '$ACTION' already selected, can't set to '$1'"
  fi
}

_is_domain() {

  if [ "$1" == "all" ]; then _error "Please specify a domain."; fi

  _debug "Checking if '$1' is a valid domain..."
  if [[ $1 =~ ^[0-9a-z\.-]+$ ]] && [[ $1 == *"."* ]] && [[ $1 != "."* ]] && [[ $1 != *"." ]]; then
    _debug "  $1 is valid"
  else
    _error "'$1' is not a valid domain"
  fi
}

_is_email() {

  if [ "$1" == "all" ]; then _error "Please specify an email address."; fi

  _debug "Checking if '$1' is a valid email address..."
  readarray -d @ -t part < <(printf '%s' "$1")
  _is_domain ${part[1]}

  _debug "Checking if '${part[0]}' is valid"
  if [[ ${part[0]} =~ ^[0-9a-zA-Z._%+-]+$ ]]; then
    _debug "'${part[0]}' is valid"
  else
    _error "'${part[0]}' is not valid a valid mailbox name."
  fi

}

_is_database() {

  if [ "$1" == "all" ]; then _error "Please specify a database."; fi

  _debug "Checking if '$1' is a valid database... "

  if [[ $1 =~ ^[0-9A-Za-z_]+$ ]] ; then #&& [[ $(expr length $1) -le 32 ]]; then
    _debug "  $1 is valid"
  else
    _error "'$1' is not a valid database name"
  fi
}

##############################################################################
#                                      Base                                  #
##############################################################################

_base_none() {
  echo "Sympl command-line - a basic parser for common system administraton tasks.

Usage: sympl [function] [action] [target]

Where function is one of:
  web, mail, mysql, dns, ftp, cron, backup or blank for the system itself.
And action is one of:
  create, enable, disable, audit, update, destroy, set <item|option> <value>
And target is the domain/email address/database/etc.

Useful things:
 - Other than using set, the function, action and target can be in any order.
 - 'set' requires two arguments immediately after it, the option and value.
 - If more permissions are needed, you'll be prompted for your password.
 - 'destory' will only destroy something which has already been disabled.

Examples:

  sympl web create example.com

  sympl mysql disable example_db

  sympl enable mail username@example.com

  sympl audit backup

  sympl destroy cron example.com

  sympl example.com set only-ssl on

  sympl update

Note: Not all functions are implimented yet.
      Please report bugs via http://bugs.sympl.io.
"

}

_base_version() {
  dpkg -l 'sympl-*' | grep '^ii' | awk '{printf $2 " " $3 "\n"};' | sort -h | while read package version; do printf "%-20s %10s\n" $package $version ; done
}

_base_create() {
  _noop
}

_base_enable() {
  _noop
}

_base_disable() {
  _noop
}

_base_destroy() {
  _noop
}

_base_set() {
  _noop
}

_base_audit() {
  _noop
}

_base_update() {
  _debug "Updating Apt"
  sudo apt-get -qq update
  _debug "Upgrading installed packages"
  sudo apt-get -q -y install --only-upgrade 'sympl-*'
}

##############################################################################
#                                       Web                                  #
##############################################################################

_web_none() {
  _noop
}

_web_create() {

  _is_domain $1

  _debug "Check for disabled sites"
  if [ -d /srv/__disabled__$1/public/htdocs ]; then
    _error "The domain $1 is disabled. Use 'sympl enable $1' to enable it."
  elif [ -d /srv/$1/public/__disabled__htdocs ] ; then
    _error "The website $1 is disabled. Use 'sympl web enable $1' to enable it."
  fi

  _debug "Check to see if $1 already exists"
  if [ -d /srv/$1/public/htdocs ]; then
    _error "$1 Already exists."
  fi

  _verbose "Creating Website for $1 at /srv/$1/public/htdocs..."
  if [ ! -d "/srv/$1" ]; then
    mkdir -p "/srv/$1"
    chown -R sympl:sympl "/srv/$1" > /dev/null 2>&1
  fi
  mkdir -p "/srv/$1/public/htdocs"
  chmod -R 2775 "/srv/$1/public" > /dev/null 2>&1
  chown -R www-data:www-data "/srv/$1/public" > /dev/null 2>&1

}

_web_enable() {

  _is_domain $1

  _debug "Checking $1 is disabled and exists"
  if [ -d /srv/$1/public/htdocs ] || [ ! -d /srv/$1/public/__disabled__htdocs ]; then
    _error "The website $1 is already enabled."
  elif [ ! -d /srv/$1 ]; then
    _error "The domain $1 does not exist."
  fi

  _verbose "Enabling Website $1"
  mv /srv/$1/public/__disabled__htdocs /srv/$1/public/htdocs

}

_web_disable() {

  _is_domain $1

  _debug "Checking $1 is enabled and exists"
  if [ -d /srv/$1/public/__disabled__htdocs ]; then
    _error "The website $1 is already disabled."
  elif [ ! -d /srv/$1/public/htdocs ]; then
    _error "The website $1 does not exist."i
  fi

  _verbose "Disabling Website $1"
  mv /srv/$1/public/htdocs /srv/$1/public/__disabled__htdocs

}

_web_destroy() {

  _is_domain $1

  _debug "Confirm '$1' is disabled and exists"
  if [ -d /srv/$1/public/htdocs ]; then
    _error "The website '$1' is enabled. It must be disabled before it can be destroyed."
  elif [ ! -d /srv/$1/public/__disabled__htdocs ]; then
    _error "The website '$1' does not exist."
  elif [ ! -d /srv/$1 ]; then
    _error "The domain '$1' does not exist."
  fi

  _verbose "Destroying /srv/$1/public/__disabled__htdocs..."
  rm -r /srv/$1/public/__disabled__htdocs/
}

_web_audit() {

  # TODO: List details of specific site, otherwise list all sites and any aliasses.

  if [ "$1" != "all" ]; then _is_domain $1 ; fi

  ( umask 077; touch /dev/shm/sympl_web_audit )
  find /srv/*/public/ -type d -name '*htdocs' > /dev/shm/sympl_web_audit

  _verbose "Enabled sites:"
  grep -v __disabled__ /dev/shm/sympl_web_audit | cut -d '/' -f 3 | sort

  _verbose "Disabled sites:"
  grep __disabled__ /dev/shm/sympl_web_audit | cut -d '/' -f 3 | sort

  rm /dev/shm/sympl_web_audit
}

_web_set() {
  _noop
}

_web_update() {
  _noop
}

##############################################################################
#                                Mysql/MariaDB                               #
##############################################################################

_mysql_none() {
  _noop
}

_mysql_create() {

  _is_database $1

  _debug "Check if $1 already exists"
  if [ $( mysql -NBe 'show databases;' | grep -c "^$1$" ) -eq 0 ]; then
    if [ $( mysql -NBe "use mysql; select User from user;" | grep -c "^$1$" ) -eq 0 ]; then
      _verbose "Creating database '$1'..."
      password="$( openssl rand -base64 32 | cut -c 1-32 )"
      _debug "Password '$password', creating DB -- CREATE DATABASE $1;"
      mysql -e "CREATE DATABASE $1;"
      _debug 'Creating user -- GRANT ALL PRIVILEGES ON $1.* TO '$1'@'localhost' IDENTIFIED BY '$password';'
      mysql -e "GRANT ALL PRIVILEGES ON $1.* TO '$1'@'localhost' IDENTIFIED BY '$password';"
      _debug 'Outputting password'
      echo "$password" >> /home/sympl/mysql_$1_password
      _debug 'Securing file'
      chmod 600 /home/sympl/mysql_$1_password
      chown sympl:sympl /home/sympl/mysql_$1_password
      _verbose "Database '$1' created with user '$1' and password '$password'"
      _verbose "  saved to /home/sympl/mysql_$1_password"
    else
      _error "User '$1' already exists."
    fi
  else
    _error "Database '$1' already exists."
  fi
}

_mysql_enable() {

  _is_database $1

  _debug "Check if $1 exists and is disabled"
  if [ $( mysql -NBe 'show databases;' | grep -c "^$1$" ) -eq 1 ]; then
    if [ $( mysql -NBe "use mysql; select User from user;" | grep -c "^$1$" ) -eq 1 ]; then
      if [ $( mysql -NBe "SHOW GRANTS for '$1'@'localhost'" 2> /dev/null | grep -c "^GRANT ALL PRIVILEGES ON \`$1\`.* TO '$1'@'localhost'$" ) -eq 0 ]; then
        _verbose "Enabling user '$1' for database '$1'"
        mysql -e "GRANT ALL PRIVILEGES ON $1.* TO '$1'@'localhost'"
      else
        _error "User '$1' is already enabled for database '$1'."
      fi
    else
      _error "User '$1'@'localhost' does not exist, although the database does."
    fi
  else
    _error "Database '$1' does not exist."
  fi

}

_mysql_disable() {

  _is_database $1

  _debug "Check if $1 exists and is enabled"
  if [ $( mysql -NBe 'show databases;' | grep -c "^$1$" ) -eq 1 ]; then
    if [ $( mysql -NBe "use mysql; select User from user;" | grep -c "^$1$" ) -eq 1 ]; then
      if [ $( mysql -NBe "SHOW GRANTS for '$1'@'localhost'" 2> /dev/null | grep -c "^GRANT ALL PRIVILEGES ON \`$1\`.* TO '$1'@'localhost'$" ) -eq 1 ]; then
        _verbose "Disabling user '$1' for database '$1'"
        mysql -e "REVOKE ALL PRIVILEGES, GRANT OPTION FROM '$1'@'localhost';"
      else
        _error "Database '$1' is already disabled for database '$1'."
      fi
    else
      _error "User '$1'@'localhost' does not exist, although the database does."
    fi
  else
    _error "Database '$1' does not exist."
  fi

}

_mysql_destroy() {

  _is_database $1

  _debug "Check if $1 exists and is disabled"
  if [ $( mysql -NBe 'show databases;' | grep -c "^$1$" ) -eq 1 ]; then
    if [ $( mysql -NBe "use mysql; select User from user;" | grep -c "^$1$" ) -eq 1 ]; then
      if [ $( mysql -NBe "SHOW GRANTS for '$1'@'localhost'" 2> /dev/null | grep -c "^GRANT ALL PRIVILEGES ON \`$1\`.* TO '$1'@'localhost'$" ) -eq 0 ]; then
        _verbose "Destroying database and user '$1'"
        mysql -e "REVOKE ALL PRIVILEGES, GRANT OPTION FROM '$1'@'localhost';"
        mysql -e "DROP USER '$1'@'localhost';"
        mysql -e "DROP DATABASE $1;"
      else
        _error "Database '$1' is not disabled. Please disable it with 'sympl mysql disable $1'"
      fi
    else
      _error "User '$1' does not exist, but the database does."
    fi
  else
    _error "Database '$1' does not exist."
  fi
}

_mysql_audit() {
  mysql -e 'show databases;'
  mysql -e 'use mysql; select User from user;'
}

_mysql_update() {
  _noop
}

##############################################################################
#                                     DNS                                    #
##############################################################################

_dns_none() {
  _noop
}

_dns_create() {
  _noop
}

_dns_enable() {
  _noop
}

_dns_disable() {
  _noop
}

_dns_destroy() {
  _noop
}

_dns_set() {
  _noop
}

_dns_audit() {
  _noop
}

_dns_update() {
  _noop
}

##############################################################################
#                                     Mail                                   #
##############################################################################

_mail_none() {
  _noop
}

_mail_create() {
  _noop
}

_mail_enable() {
  _noop
}

_mail_disable() {
  _noop
}

_mail_destroy() {
  _noop
}

_mail_set() {
  _noop
}

_mail_audit() {
  _noop
}

##############################################################################
#                                     FTP                                    #
##############################################################################

_ftp_none() {
  echo "Sympl FTP command line interface

Usage: sympl ftp [action] [target|user@target]

Where action is one of:
  create, enable, disable, reset, destroy, set, audit
And target is the domain or user to work on.

Where create is the action:
  the domain folder will be created if it is not already present
Where enable is the action:
  the password will be enabled if disabled and set if not present
Where disable is the action:
  FTP Access will be disabled for per-domain or user access
Where reset is the action:
  The password will be reset either per-domain or per-user, even if the
  domain or user are disabled
Where destroy is the action:
  FTP Access will be removed for both per-domain and per-user access assuming
  accounts have been disabled
Where set is the action:
  you can restrict per-user access for the user to a specific folder (by
  default access is granted to the root of the public folder for that domain)
  or by quota. the '-' character can be used to clear the set value for both
  the folder and the quota so the folder reverts back to the domains public
  folder and the quota reverts back to unlimited.

Examples:

  sympl ftp create example.com

  sympl ftp enable example.com

  sympl ftp disable example.com

  sympl ftp destroy user2@example.com

  sympl ftp audit all|example.com

  sympl ftp enable user1@example.com

  sympl ftp reset user2@example.com

  sympl ftp set folder <folder name> user2@example.com

  sympl ftp set quota <10K|M|G> user2@example.com
"
}

_ftp_create() {

  if [[ "$1" =~ .*"@".* ]]; then
    _is_email $1
    readarray -d @ -t part < <(printf '%s' "$1")
    mailbox=${part[0]}
    domain=${part[1]}
    else
    _is_domain $1
    domain=$1
  fi

  _debug "Check for disabled sites"
  if [ -d /srv/__disabled__$domain/public ]; then
    _error "The domain '$domain' is disabled. Use 'sympl enable $domain' to enable it."
  fi

  _debug "Check to see if '$domain' already exists"
  if [ -d /srv/$domain/public ]; then
    _error "$domain Already exists. No need to continue"
  fi

  _verbose "Creating FTP folder for '$domain' at /srv/$domain/public/"
  mkdir -p "/srv/$domain/public"
  chmod -R 2775 "/srv/$domain/public" > /dev/null 2>&1
  chown -R sympl: "/srv/$domain/public" > /dev/null
}

_ftp_enable() {

  if [[ "$1" =~ .*"@".* ]]; then
    _is_email $1
    readarray -d @ -t part < <(printf '%s' "$1")
    mailbox=${part[0]}
    domain=${part[1]}
    else
    _is_domain $1
    domain=$1
  fi


  _debug "Check if domain is set up"
  if [ ! -d /srv/$domain ]; then
    _error "The domain $domain does not exist."
  fi

  _debug "Check if the domains config folder exists"
  if [ ! -d /srv/$domain/config ]; then
    _verbose "Creating the config folder for domain '$domain'."
    mkdir -p /srv/$domain/config
  fi

  if [ "x$mailbox" != "x"  ]; then

    _debug "Check for the ftp-users file and create a blank one if it is absent."
    if [ ! -f /srv/$domain/config/ftp-users ]; then
      _verbose "Setting up ftp-users file"
      echo "#username:password:folder:quota" > /srv/$domain/config/ftp-users
      chown sympl: /srv/$domain/config/ftp-users
    fi

    _debug "Checking for user $mailbox in domain $domain"
    if [ "$( grep -R ^$mailbox: /srv/$domain/config/ftp-users )" ]; then
      _error "FTP user $mailbox already enabled on domain $domain."
    elif [ "$( grep -R ^#$mailbox: /srv/$domain/config/ftp-users )" ]; then
      _verbose "Enabling disabled user '$mailbox' on domain '$domain'."
      sed -ri "/^#$mailbox/s/#//" /srv/$domain/config/ftp-users
    else
      _verbose "Adding user '$mailbox' to domain '$domain'."
      password="$( openssl rand -base64 32 | sed 's/\///g' | cut -c 1-32 )"
      echo "$mailbox:$password" >> /srv/$domain/config/ftp-users
    fi

  else

    _debug "Check if per-domain access is already enabled"
    if [ -f /srv/$domain/config/ftp-password ]; then
      _error "The domain $domain is already enabled for FTP access, to reset the pasword use <sympl ftp reset $domain>."
    fi
    _debug "Check for disabled per-domain access for '$domain'"
    if [ -f /srv/$domain/config/__disabled__ftp-password ]; then
      _verbose "Re-enabling per-domain ftp access for $domain."
      mv /srv/$domain/config/__disabled__ftp-password /srv/$domain/config/ftp-password
    else
      _verbose "Setting per-domain FTP password for '$domain'"
      password="$( openssl rand -base64 32 | cut -c 1-32 )"
      _debug "Password '$password' for user $domain on domain $domain;"
      echo "$password" > /srv/$domain/config/ftp-password
      _verbose "Access created for '$domain' with password '$password'"
      chown sympl: "/srv/$domain/config/ftp-password"
    fi
  fi

}

_ftp_disable() {

  if [[ "$1" =~ .*"@".* ]]; then
    _is_email $1
    readarray -d @ -t part < <(printf '%s' "$1")
    mailbox=${part[0]}
    domain=${part[1]}
    else
    _is_domain $1
    domain=$1
  fi

  if [ ! -d /srv/$domain ]; then
    _error "The domain '$domain' does not exist."
  fi

  if [ "x$mailbox" != "x"  ]; then

    _debug "Checking for user $mailbox in domain $domain"
    if [ "$( grep -R ^#$mailbox: /srv/$domain/config/ftp-users )" ]; then
      _error "FTP user $mailbox already disabled on domain $domain."
    elif [ "$( grep -R ^$mailbox: /srv/$domain/config/ftp-users )" ]; then
      _verbose "Disabling user '$mailbox' on domain '$domain'."
      sed -ri "s/(^$mailbox:)/#\1/" /srv/$domain/config/ftp-users
      return 0;
    else
      _error "User '$mailbox' not found for domain '$domain'."
    fi

  else

    _debug "Checking per-domain ftp access for $domain is enabled and exists"
    if [ -f /srv/$domain/config/__disabled__ftp-password ]; then
      _error "The per-domain ftp access for $domain is already disabled."
    elif [ ! -f /srv/$domain/config/ftp-password ]; then
      _error "The per-domain ftp access for $domain does not exist."
    fi

    _verbose "Disabling per-domain FTP access for $domain"
    mv /srv/$domain/config/ftp-password /srv/$domain/config/__disabled__ftp-password

  fi
}

_ftp_reset() {

  if [[ "$1" =~ .*"@".* ]]; then
    _is_email $1
    readarray -d @ -t part < <(printf '%s' "$1")
    mailbox=${part[0]}
    domain=${part[1]}
  else
    _is_domain $1
    domain=$1
  fi

  if [ -d /srv/$domain ]; then
    _error "The domain '$domain' does not exist."
  fi

  if [ "x$mailbox" != "x" ]; then

    _debug "Checking for per user access on domain '$domain'"
    password="$( openssl rand -base64 32 | sed 's/\///g' | cut -c 1-32 )"
    if [ ! -f /srv/$domain/config/ftp-users ]; then
      _error "per user access not set up for domain '$domain'. please use <sympl ftp enable $mailbox@$domain>."
    elif [ "$( grep -R ^#*$mailbox: /srv/$domain/config/ftp-users )" ]; then
      if [ "$( grep -R ^#$mailbox: /srv/$domain/config/ftp-users )" ]; then
        _verbose "Resetting password for disabled user '$mailbox' on '$domain' to '$password'."
      else
        _verbose "Resetting password for '$mailbox' on '$domain' to '$password'."
      fi
      sed -ri "s/(^#*$mailbox:)[^:]*(.*)/\1$password\2/" /srv/$domain/config/ftp-users
      return 0;
    else
      _error "User '$mailbox' not foung for per-user access on domain '$domain'."
    fi

  else

    _debug "Resetting the per-domain access password for $domain"
    password="$( openssl rand -base64 26 | cut -c 1-32 )"
    _debug "Checking if per-domain ftp access for $domain exists"
    if [ -f /srv/$domain/config/__disabled__ftp-password ]; then
      _debug "The per-domain ftp access for $domain is disabled. Resetting password"
      echo "$password" > /srv/$domain/config/__disabled__ftp-password
      _verbose "Setting the disabled per-domain FTP access password for '$domain' to $password"
      return 0;
    elif [ ! -f /srv/$domain/config/ftp-password ]; then
      _error "The per-domain ftp access for $domain does not exist."
    fi

    _verbose "Resetting the password for per-domain FTP access for $domain"
    printf '%s' "$password" > /srv/$domain/config/ftp-password

  fi

}

_ftp_destroy() {

  if [[ "$1" =~ .*"@".* ]]; then
    _is_email $1
    readarray -d @ -t part < <(printf '%s' "$1")
    mailbox=${part[0]}
    domain=${part[1]}
    else
    _is_domain $1
    domain=$1
  fi

  if [ ! -d /srv/$domain ]; then
    _error "The domain '$domain' does not exist."
  fi

  if [ "x$mailbox" != "x" ]; then
    _debug "Confirm '$mailbox' is disabled and exists."
    if [ "$( grep -R ^$mailbox: /srv/$domain/config/ftp-users )" ]; then
      _error "The user '$mailbox' is enabled for domain '$domain'. It must be disabled before it can be destroyed."
    elif [ "$( grep -R ^#mailbox: /srv/$domain/config/ftp-users )" ]; then
      _verbose "destroying user '$mailbox' for domain '$domain'."
      sed -ri "/^#$mailbox:/d" /srv/$domain/config/ftp-users
      return 0;
    elif [ ! -f /srv/$domain/config/ftp-users ]; then
      _error "Per-user access is not set up for the domain '$domain'."
    else
      _error "User '$mailbox' not found for domain '$domain'"
    fi

  else

    _debug "Confirm '$domain' is disabled and exists"
    if [ -f /srv/$domain/config/ftp-password ]; then
      _error "The ftp access for '$domain' is enabled. It must be disabled before it can be destroyed."
    elif [ ! -f /srv/$domain/config/__disabled__ftp-password ]; then
      _error "The ftp access for '$domain' does not exist."
    fi

    _verbose "Destroying /srv/$domain/config/__disabled__ftp-password..."
    rm -r /srv/$domain/config/__disabled__ftp-password

  fi
}

_ftp_set() {

  _is_email $1
  readarray -d @ -t part < <(printf '%s' "$1")
  mailbox=${part[0]}
  domain=${part[1]}

  if [ -z "$3" ]; then
    _error "Set requires quota or folder to have a size or location argument"
  fi

  _debug "Check if domain is set up for FTP"
  if [ ! -d /srv/$domain ]; then
    _error "The domain $domain does not exist."
  elif [ ! -d /srv/$domain/public ]; then
    _error "The domain $domain is not set up for FTP access. Please use <sympl ftp create $domain>"
  fi

  _debug "Check if the domains config folder exists"
  if [ ! -d /srv/$domain/config ]; then
    _verbose "Creating the config folder for domain $domain."
    mkdir -p /srv/$domain/config
  fi


  if [[ "$2" == "folder" ]]; then
    _debug "Checking for user $mailbox"
    if [ "$3" == "-" ]; then set -- "${@:1:2}"; fi
    if [ "$( grep -R ^#*$mailbox: /srv/$domain/config/ftp-users )" ]; then
      _debug "Setting folder for $mailbox on $domain to $3"
      while read line; do
        readarray -d : -t item < <(printf '%s' "$line")
        sed -ri "s/(^#*$mailbox:).*/\1${item[1]}:$3:${item[3]}/" /srv/$domain/config/ftp-users
      done <<< $( grep -R "^#*$mailbox:" /srv/$domain/config/ftp-users )
      _verbose "Folder reset for '$mailbox' on domain '$domain' to '$3'"
      return 0;
    else
      _error "User $mailbox not found in configuration, please use <sympl ftp enable username@domain>."
    fi
  fi

  if [[ "$2" == "quota" ]]; then
    _debug "Checking for user $mailbox"
    if [ "$3" == "-" ]; then set -- "${@:1:2}"; fi
    if [ "$( grep -R ^#*$mailbox: /srv/$domain/config/ftp-users )" ]; then
      _debug "Setting quota for $mailbox on $domain to $3"
      while read line; do
        readarray -d : -t item < <(printf '%s' "$line")
        sed -ri "s/(^#*$mailbox:).*/\1${item[1]}:${item[2]}:$3/" /srv/$domain/config/ftp-users
      done <<< $( grep -R "^#*$mailbox:" /srv/$domain/config/ftp-users )
      _verbose "Quota reset for '$mailbox' on domain '$domain' to '$3'"
      return 0;
    else
      _error "User $mailbox not found in configuration, please use <sympl ftp set username@domain>."
    fi
  fi

}

_ftp_audit() {

  if [ "$1" != "all" ]; then _is_domain $1 ; else set "*"; fi

  ( umask 077; touch /dev/shm/sympl_ftp_audit )
  find /srv/$1/config/ -type f -name '*ftp-password' > /dev/shm/sympl_ftp_audit
  find /srv/$1/config/ -type f -name 'ftp-users' >> /dev/shm/sympl_ftp_audit

  _verbose "Enabled per-domain access:"
  dom=$( grep '/ftp-password' /dev/shm/sympl_ftp_audit | cut -d '/' -f 3 | sort )
  [ -z "$dom" ] && echo "None found." || echo "$dom"; unset dom

  _verbose "Disabled per-domain access:"
  dom=$( grep __disabled__ftp-password /dev/shm/sympl_ftp_audit | cut -d '/' -f 3 | sort )
  [ -z "$dom" ] && echo "None found." || echo "$dom"; unset dom

  for domain in $( grep ftp-users /dev/shm/sympl_ftp_audit | cut -d '/' -f 3 | sort ); do
    _verbose "Per-user access exists for domain $domain:"
    printf '\t%-25s%-10s%-10s\n' "<username>" "<folder>" "<quota>"
    while read line; do
      readarray -d : -t item <<< "$line"
      printf '\t%-25s%-10s%-10s\n' "${item[0]}@$domain" "${item[2]}" "${item[3]::-1}"
    done <<< $( sed '/^#.*username/d' /srv/$domain/config/ftp-users )
  done
  _debug "Cleaning up audit file"
  rm /dev/shm/sympl_ftp_audit

}

_ftp_update() {
  _noop
}

##############################################################################
#                                    Backup                                  #
##############################################################################

_backup_none() {
  _noop
}

_backup_create() {
  _verbose "Taking backup..."
  sudo backup2l -b
}

_backup_enable() {
  if [ ! -x /etc/cron.daily/zz-backup2l ]; then
    _verbose "Enabling backups."
    sudo chmod +x /etc/cron.daily/zz-backup2l
  else
    _error "Backups are already enabled."
  fi
}

_backup_disable() {
  _verbose "Disabling backups."
  if [ -x /etc/cron.daily/zz-backup2l ]; then
    _verbose "Disabling backups."
    sudo chmod -x /etc/cron.daily/zz-backup2l
  else
    _error "Backups are already disabled."
  fi
}

_backup_destroy() {
  if [ $( backup2l -s | grep -c '^all.2 ' ) -ge 1 ]; then
    _verbose "Removing oldest backup set."
    sudo backup2l -p 1 > /dev/null
  else
    _error "Not removing only backup set."
  fi
}

_backup_set() {
  _error "Backup has no settings."
}

_backup_audit() {
  _verbose "Backup status:"
  backup2l -s
}

_backup_audit() {
  _noop
}

##############################################################################
#                                     Cron                                   #
##############################################################################

_cron_none() {
  _noop
}

_cron_create() {
  _noop
}

_cron_enable() {
  _noop
}

_cron_disable() {
  _noop
}

_cron_destroy() {
  _noop
}

_cron_set() {
  _noop
}

_cron_audit() {
  _noop
}

_cron_update() {
  _noop
}

##############################################################################
#                                  NoOp Stub                                 #
##############################################################################


_noop() {
  _warn "Sorry this doesn't do anything yet, but it may be in a new version.\n        Run 'sympl update' for an updated version."
  exit 0
}

##############################################################################
#                              Handle Command Line                           #
##############################################################################

_main() {

  UNHANDLED=()
  while [ $# -gt 0 ]; do
    case $1 in
      --debug)                  DEBUG=TRUE               ; shift ;;
      --quiet|-q)               VERBOSE=FALSE            ; shift ;;
      web|http|website)         _func web                ; shift ;;
      mail|email)               _func mail               ; shift ;;
      mysql|database|mariadb)   _func mysql              ; shift ;;
      dns|domain)               _func dns                ; shift ;;
      backup)                   _func backup             ; shift ;;
      cron)                     _func cron               ; shift ;;
      ftp)                      _func ftp                ; shift ;;
      create)                   _action 'create'         ; shift ;;
      destroy)                  _action 'destroy'        ; shift ;;
      enable)                   _action 'enable'         ; shift ;;
      disable)                  _action 'disable'        ; shift ;;
      audit|list)               _action 'audit'          ; shift ;;
      reset)                    _action 'reset'		       ; shift ;;
      update|upgrade)           _action 'update'         ; shift ;;
      version|versions|-v)      _action 'version'        ; shift ;;
      set)                      _action 'set' "$2" "$3"  ; shift ; shift ; shift ;;
      *) # unhandled parameter
        if [ $TARGET = all ]; then
          TARGET="$1"
        else
          UNHANDLED+=("$1")
        fi
        shift # past argument
      ;;
    esac
  done

  _debug "Command line parameters: $@"

  _debug "Running as '$I_AM', EUID '$EUID'"

  _debug "Runtime variables:
  Unhanded         | $UNHANDLED
  FUNCTION         | $FUNCTION
  ACTION           | $ACTION
  TARGET           | $TARGET
  OPTION           | $OPTION
  SETTING          | $SETTING"

  if [ "x$UNHANDLED" != "x" ]; then
    _error "Unexpected input: '$UNHANDLED', run 'sympl' for help."
  fi

  _${FUNCTION}_${ACTION} $TARGET $OPTION $SETTING

}


_main $@

exit 0

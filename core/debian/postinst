#!/bin/bash
#

set -e

# Add the expected locales if not already added
if [ $( grep -c '^en_GB.UTF-8 UTF-8' /etc/locale.gen 2> /dev/null ) -ne 1 ]; then
  echo -e "# below added by Sympl\nen_GB.UTF-8 UTF-8\n# above added by Sympl" >> /etc/locale.gen
  /usr/sbin/locale-gen
fi
if [ $( grep -c '^en_US.UTF-8 UTF-8' /etc/locale.gen 2> /dev/null ) -ne 1 ]; then
  echo -e "# below added by Sympl\nen_US.UTF-8 UTF-8\n# above added by Sympl" >> /etc/locale.gen
  /usr/sbin/locale-gen
fi

#
# Skip, if we are not in "configure" state
#
if [ "$1" != "configure" ]; then
        echo "I: Skipping configuration"
        exit 0
fi

#
# Move the standard Debian MOTD
#

if [ -f /etc/motd ] && [ $( grep -c '^The programs included with the Debian' /etc/motd ) -gt 0 ]; then
  echo "I: Moving stock MOTD"
  mv /etc/motd /etc/motd.dpkg-sympl-orig
  echo "" /etc/motd
fi

#
# Double check this file gets installed with the correct permissions
#
if ( ! dpkg-statoverride --list /etc/sudoers.d/sympl > /dev/null ) ; then
  dpkg-statoverride --add --update root root 0440 /etc/sudoers.d/sympl
fi

#
# Shadow passwords must be on.
#
shadowconfig on

#
#  If there isn't a Sympl account, add it with password/keys from root.
#
if ( ! grep ^sympl: /etc/passwd 2>/dev/null >/dev/null ); then

  adduser --home=/home/sympl --shell=/bin/bash --disabled-login --gecos='Sympl Administrator,,,' sympl

  #
  #  Now set the password for Sympl to that used by root.
  #
  usermod -p "$(grep root /etc/shadow | cut -f 2 -d :)" sympl

  #
  # If the root user has authorized_keys, copy them to the new Sympl user
  #
  if [ -f /root/.ssh/authorized_keys ]; then
    mkdir -p /home/sympl/.ssh
    cp /root/.ssh/authorized_keys /home/sympl/.ssh/authorized_keys
    chown sympl:sympl /home/sympl/.ssh/authorized_keys
  fi

fi

#
# Check that the adm and www-data groups exist, then add the sympl user to them
# Fixes Issue #271, where the Sympl user already exists.
#

for GROUP in adm www-data ; do

  if ( getent group $GROUP >/dev/null ); then
    if [ $( id sympl | grep -c "($GROUP)" ) == 0 ]; then
      adduser sympl $GROUP
    fi
  fi

done

#
# Add the hostname along with the public IP to /etc/hosts if we don't have a full hostname
#
if [ "x$( hostname -f 2> /dev/null )" == "x" ] ; then
  echo "No full hostname - updating /etc/hosts"
  etc_hosts=$(cat /etc/hosts)
  if [[ "$etc_hosts" != *"."* ]]; then
    etc_hosts="$etc_hosts.localdomain"
  fi
  echo -e "$( sympl-ip )\t$HOSTNAME $(hostname -s)\n$(cat /etc/hosts)" > /etc/hosts
  echo "Full hostname now '$(hostname -f)'."
fi

#
# Add a stat override for the /srv directory.
#
if ( ! dpkg-statoverride --list /srv > /dev/null ) ; then
  dpkg-statoverride --add --update sympl sympl 2755 /srv
fi

#
# Set the hostname, preferring the FQDN if it's there.
#
# We default to what it's been set to at the moment, rather than
#   what's in /etc/hostname, as its easy to change one but forget the other.
#
if hostname --fqdn > /dev/null ; then
  _HOSTNAME="$( hostname --fqdn )"
else
  _HOSTNAME="$( hostname )"
fi
echo "I: Hostname is $_HOSTNAME"

#
# Append ".localdomain" if HOSTNAME has no dots
# (which is unlikely to happen with a clean install)
#
if ! [[ "$_HOSTNAME" =~ ^[_a-z0-9-]+\.([_a-z0-9-]+\.?)+$ ]] ; then
  echo "I: Hostname is not an FQDN, changing to $_HOSTNAME.localdomain."
  _HOSTNAME="$_HOSTNAME.localdomain"
fi

#
# If the full hostname isn't now in /etc/hosts, then add it
# 
# This assumes that theres only one entry for each IP, and doesn't deal with
#   partially mangled hosts files, but will deal with someone changing the
#   hostname but not also changing /etc/hosts
#
#if [ $( grep -c $_HOSTNAME '/etc/hosts' ) == 0 ]; then
#  echo "I: Updating hostname configuration with complete name."
#  if hostname -i > /dev/null ; then
#    hostname_ips="$( hostname -i )"
#  else
#    hostname_ips="127.0.1.1"
#  fi
#  sed -i "s|^$hostname_ips|# $hostname_ips|" '/etc/hosts'
#  sed -i "1i$hostname_ips\t$_HOSTNAME $( echo $_HOSTNAME | cut -d '.' -f 1 )" '/etc/hosts'
#  export HOSTNAME="$_HOSTNAME"
#  hostname -b "$_HOSTNAME"
#fi

#
# Enforce using the full hostname
#
#echo "I: Checking hostname configuration files."
#if [ "$HOSTNAME" != "$_HOSTNAME" ]; then export HOSTNAME="$_HOSTNAME"; fi
#if [ "$( hostname )" != "$_HOSTNAME" ]; then hostname -b "$_HOSTNAME" ; fi
#if [ "$( cat /etc/hostname )" != "$_HOSTNAME" ]; then echo "$_HOSTNAME" > "/etc/hostname" ; fi 
#if [ -f "/etc/mailname" ] && [ "$( cat "/etc/mailname" )" != "$_HOSTNAME" ]; then echo "$_HOSTNAME" > "/etc/mailname" ; fi

#
#  If there are no existing directories beneath /srv/ create the defaults.
#
if [ ! -e "/srv/$_HOSTNAME" ] ; then
  #
  # Create the standard directories
  # 
  mkdir -p /srv/$_HOSTNAME/public/htdocs
  mkdir -p /srv/$_HOSTNAME/public/logs
  mkdir -p /srv/$_HOSTNAME/config
  mkdir -p /srv/$_HOSTNAME/mailboxes/root

  # With the right permissions
  chown -R sympl:sympl /srv/$_HOSTNAME
  chown -R www-data:www-data /srv/$_HOSTNAME/public
fi

#
# We'd like to generate a certificate for the hostname.  Naturally this will go in /srv/$_HOSTNAME
#
if [ -d "/srv/$_HOSTNAME/config" ] ; then
  #
  # Generate certificates for this host
  #
  if ! ( sympl-ssl $_HOSTNAME ) ; then
    echo "W: SSL certificate generation failed. Generating a temporary self-signed certificate..."
    echo selfsigned > /srv/$_HOSTNAME/config/ssl-provider
    sympl-ssl $_HOSTNAME || true
    rm /srv/$_HOSTNAME/config/ssl-provider
  fi
fi

#
# Not interested in linking from /etc/$_HOSTNAME/config/ssl.*
#
ssl_current_dir="/srv/$_HOSTNAME/config/ssl/current"

#
# If there are no cerificates in /etc/ssl, symlink those from this directory.
# This is race-tastic.
#
if [ ! -e "/etc/ssl/ssl.key" ] &&
    [ ! -e "/etc/ssl/ssl.crt" ] &&
    [ ! -e "/etc/ssl/ssl.combined" ] &&
    ( sympl-ssl --no-generate --no-rollover $_HOSTNAME ) &&
    [ -e "$ssl_current_dir/ssl.key" ] && 
    [ -e "$ssl_current_dir/ssl.crt" ] && 
    [ -e "$ssl_current_dir/ssl.combined" ] ; then

  echo "I: Symlinking SSL certificate and key from $ssl_current_dir to /etc/ssl"
  ln -s "$ssl_current_dir"/ssl.{key,crt,combined} /etc/ssl/

  #
  # Move existing bundle out of the way.
  #
  if [ -e /etc/ssl/ssl.bundle ] ; then
    echo "W: Moving old SSL bundle to /etc/ssl/ssl.bundle.$$"
    mv /etc/ssl/ssl.bundle /etc/ssl/ssl.bundle.$$        # ugh
  fi

  #
  # Link any new bundle in again.
  #
  if [ -e "$ssl_current_dir/ssl.bundle" ] ; then
    echo "I: Symlinking SSL bundle from $ssl_current_dir to /etc/ssl"
    ln -s "$ssl_current_dir/ssl.bundle" /etc/ssl/ssl.bundle
  fi

fi

# Run sympl-filesystem-security to enforce permissions
sympl-filesystem-security

# Enable Buster Backports, and pin the needed packages for sympl-phpmysql
if [ "$(lsb_release -c -s)" == "buster" ]; then
  if [ $( cat /etc/apt/sources.list /etc/apt/sources.list.d/*.list | sed 's|#.*||' | grep -c '^deb .* buster-backports' ) -lt 1 ]; then
    echo -n 'Enabling Debian buster-backports repo for phpmyadmin...'
    echo "deb http://deb.debian.org/debian buster-backports main" > /etc/apt/sources.list.d/enable_backports.list
    echo ' ok'
  fi
  if [ ! -f /etc/apt/preferences.d/sympl-phpmyadmin ]; then
    echo -n 'Enabling install of phpmyadmin from backports...'
    echo -e "Package: phpmyadmin\nPin: release a=buster-backports\nPin-Priority: 900" > /etc/apt/preferences.d/sympl-phpmyadmin
    echo -e "Package: php-twig\nPin: release a=buster-backports\nPin-Priority: 900" >> /etc/apt/preferences.d/sympl-phpmyadmin
	echo ' ok'
  fi
fi

apt-get -qq update

# Create htop defaults
if [ ! -f /home/sympl/.config/htop/htoprc ]; then
  mkdir -p /home/sympl/.config/htop/
  echo "hide_threads=0
hide_kernel_threads=1
hide_userland_threads=1
shadow_other_users=0
show_thread_names=1
show_program_path=1
highlight_base_name=1
highlight_megabytes=1
highlight_threads=1
tree_view=1
header_margin=0
detailed_cpu_time=1
cpu_count_from_zero=0
update_process_names=1
account_guest_in_cpu_meter=1" > /home/sympl/.config/htop/htoprc
  chown sympl:sympl -R /home/sympl/.config
fi

if [ ! -f /root/.config/htop/htoprc ]; then
  mkdir -p /root/.config/htop/
  echo "hide_threads=0
hide_kernel_threads=1
hide_userland_threads=1
shadow_other_users=0
show_thread_names=1
show_program_path=1
highlight_base_name=1
highlight_megabytes=1
highlight_threads=1
tree_view=1
header_margin=0
detailed_cpu_time=1
cpu_count_from_zero=0
update_process_names=1
account_guest_in_cpu_meter=1" > /root/.config/htop/htoprc
  chown root:root /root/.config/htop/htoprc
fi

# Remove rogue symlink
if [ -L /etc/cron.hourly/00-sympl-ssl ]; then rm /etc/cron.hourly/00-sympl-ssl; fi 


#DEBHELPER#

exit 0

#
# This clause rate limits authenticated clients based on the value stored in
# /srv/domain.com/mailboxes/bob/ratelimit OR
# /srv/domain.com/config/mailbox-ratelimit, with the default value being 100
# messages / hour
#

deny authenticated = *
     message       = Sender rate for $authenticated_id exceeds $sender_rate_limit messages per $sender_rate_period
     log_message   = Sender rate for $authenticated_id is $sender_rate / $sender_rate_period
     condition     = ${if and{\
                        {!eq{$authenticated_id}{}}\
                        { or {\
                          {exists{VHOST_DIR/${domain:$authenticated_id}/VHOST_CONFIG_DIR/mailbox-ratelimit}}\
                          {exists{VHOST_DIR/${domain:$authenticated_id}/VHOST_MAILBOX_DIR/${local_part:$authenticated_id}/ratelimit}}\
                        }}\
                     }}

     # if the user mailbox has a ratelimit file, then read that, else read the
     #   ratelimit from the domain, and default to 100 if the file is empty or devoid
     #   of numeric value ("Beethoven's 5th box of frogs" will return '5' as it stands ;))

     # First, detaint full path to the dominant config file. In case it looks diabolical,
     # the nest is built with two dsearch detaints, where the untrusted key is tested
     # against our known world;
     #
     #    set acl_m_dom = ${lookup {${domain:$authenticated_id}} dsearch,ret=full {VHOST_DIR}}
     #    set acl_m_mbx = ${lookup {${local_part:$authenticated_id}} dsearch,ret=full {$acl_m_dom/VHOST_MAILBOX_DIR}}
     #    set acl_m_fp1 = {$acl_m_mbx/ratelimit}
     #    set acl_m_fp2 = {$acl_m_dom/VHOST_CONFIG_DIR/mailbox-ratelimit}

     set acl_m_cfg = ${if exists{VHOST_DIR/${domain:$authenticated_id}/VHOST_MAILBOX_DIR/${local_part:$authenticated_id}/ratelimit}\
                        {${lookup {${local_part:$authenticated_id}} dsearch,ret=full\
                           {${lookup {${domain:$authenticated_id}} dsearch,ret=full {VHOST_DIR}}/VHOST_MAILBOX_DIR}}/ratelimit}\
                        {${lookup {${domain:$authenticated_id}} dsearch,ret=full {VHOST_DIR}}/VHOST_CONFIG_DIR/mailbox-ratelimit}\
                     }

     # N.B. could use [feature-detection for] match_/inlist $value to avoid file lookups
     #      under exim 4.96+, e.g., the detainted ratelimit path...
     #
     #    ${if eq{${if inlist{x}{x}{$value}}}{x}\
     #       {${if inlist{${domain:$authenticated_id}}{+local_domains}{VHOST_DIR/$value/VHOST_CONFIG_DIR/ratelimit}}}\
     #       {${lookup {${domain:$authenticated_id}} dsearch,ret=full {VHOST_DIR}}/VHOST_CONFIG_DIR/ratelimit}\
     #    }
     #
     # ... but it's untested on 4.96 and risks breaking on upgrade

     # Solid to-date but belt-and-braces
     condition     = ${if exists{$acl_m_cfg}}

     ratelimit     = ${if match\
                          {${readfile{$acl_m_cfg}}}\
                          {([0-9]+)}{$1}{100}} / 1h / strict / $authenticated_id

     # logwrite      = ACL#10/60/05: DEBUG ! $acl_m_cfg > $sender_rate_limit / hour



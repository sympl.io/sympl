# This router handles aliases for vhost users
vhost_aliases:
  debug_print = "R: vhost_aliases for $local_part@$domain"
  driver = redirect
  domains = +vhost_domains
  local_part_suffix = +*
  local_part_suffix_optional
  # Make sure the files exists to avoid awkward failures
  condition  = ${if exists{VHOST_DIR/$domain/config/aliases}}
  # this uses detaint which is a nasty hack to de-taint the submitted $local_part variable
  #   a proper fix should be made which checks the relevant values and populates 
  #   $local_parts
  data = ${lookup{${lookup{$local_part} lsearch*,ret=key{/etc/exim4/detaint}}}lsearch{VHOST_DIR/$domain_data/config/aliases}}
  # Set permissions for any actions we might take
  user  = ${extract{uid}{${stat:VHOST_DIR/$domain/VHOST_CONFIG_DIR}}}
  group = ${extract{gid}{${stat:VHOST_DIR/$domain/VHOST_CONFIG_DIR}}}
  # Set the home directory for the router/transport.
  router_home_directory    = VHOST_DIR/$domain/VHOST_MAILBOX_DIR/$local_part
  transport_home_directory = VHOST_DIR/$domain/VHOST_MAILBOX_DIR/$local_part
  # Preserve the domain if the aliases are unqualified.
  qualify_preserve_domain = true
  directory_transport = address_directory
  file_transport = address_file
  pipe_transport = address_pipe
  reply_transport = address_reply
  # these two are required to allow :fail:, :defer: to work
  allow_fail
  allow_defer
  # This stops this router from trying to change uid/gid whilst verifying the
  # address by skipping this router.  The address will get validated by the
  # next router.
  no_verify

# If the aliases file exists, and the user exists in that file, we can accept
# the message.  This router is *only* for verification.
vhost_aliases_check:
  group = ${extract{gid}{${stat:VHOST_DIR/$domain/VHOST_CONFIG_DIR}}}
  debug_print = "R: vhost_aliases_check for $local_part@$domain"
  driver = accept
  local_part_suffix = +*
  local_part_suffix_optional
  condition  = ${if and{\
        {exists{VHOST_DIR/$domain/config/aliases}}\
        {eq{${lookup{${lookup{$local_part} lsearch*,ret=key{/etc/exim4/detaint}}}lsearch{VHOST_DIR/${lookup{$domain} lsearch*,ret=key{/etc/exim4/detaint}}/config/aliases}{yes}}}{yes}}\
        }}
  verify_only


# Run our wrapper once per minute, unless the binary has gone away.
*/1 * * * * root [ -x /usr/sbin/sympl-all-crontabs ] && /usr/sbin/sympl-all-crontabs

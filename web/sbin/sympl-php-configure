#!/bin/bash

#TODO: Improve error trapping
set -e
trap '_traperror $LINENO' ERR

function _traperror {
    local lc="$BASH_COMMAND" rc=$? ln=${BASH_LINENO[$i]}
    >&2 echo "===== $0:$1 ====="
    >&2 awk 'NR>L-4 && NR<L+4 { printf "%-5d%3s%s\n",NR,(NR==L?">>>":""),$0 }' L=$1 $0
    >&2 echo "===== exited $rc ====="
}

function _debug {
    if [ "x$DEBUG" != "x" ]; then
        >&2 echo -ne "DEBUG: $@"
    fi
}

function _echo {
    if [ "x$QUIET" == "x" ]; then
        echo -ne "$@"
    fi
}

function _error {
    >&2 echo -ne "$@"
}

# check if we have a terminal, if not, then be quiet
# this works as a simple way to only output info when run by a user
if [ ! -t 1 ] ; then
    QUIET=1
fi


#TODO: Add switches:
# --quiet (for cron runs, only report problems)
# --versions (list available PHP versions)
# --modules (list available modules for PHP versions)
# --debug (more output, import exxisting functions)

#TODO: Break functions into seperate libraries

# set up overideable locations
if [ -z $SRV ] ; then SRV=/srv ; fi
if [ -z $ETC_SYMPL ] ; then ETC_SYMPL=/etc/sympl ; fi

if [ "$(whoami)" != "root" ]; then
    _error "Error: Currently must be run as root."
    exit 1
fi

# Add the key for the Sury repo if not already installed
if [ ! -f /usr/share/keyrings/deb.sury.org-php.gpg ]; then
    _debug "Adding Sury repo key"
    wget -qq -O /tmp/debsuryorg-archive-keyring.deb https://packages.sury.org/debsuryorg-archive-keyring.deb
    dpkg -i /tmp/debsuryorg-archive-keyring.deb
    rm -f /tmp/debsuryorg-archive-keyring.deb
    if [ -f /etc/apt/trusted.gpg.d/deb.sury.org-php.gpg ]; then
        _debug "Removing expired Sury repo key"
        rm -f /etc/apt/trusted.gpg.d/deb.sury.org-php.gpg
    fi
fi

# Pin the sury repo so the Debian Repo takes preference
if [ ! -f /etc/apt/preferences.d/deb_sury_org ]; then
    _debug "Setting Sury repo pinning"
    echo -e "# Prefer php-common from Sury\nPackage: *\nPin: origin \"packages.sury.org\"\nPin-Priority: 600\n" >> /etc/apt/preferences.d/deb_sury_org
    echo -e "# Prefer base Debian packages where available\nPackage: *\nPin: origin \"packages.sury.org\"\nPin-Priority: 400\n" >> /etc/apt/preferences.d/deb_sury_org
fi

# Add the Sury repo if not already added, then refresh the packages
if [ ! -f /etc/apt/sources.list.d/deb.sury.org-php.list ] || [ $( grep -c 'signed-by=/usr/share/keyrings/deb.sury.org-php.gpg' /etc/apt/sources.list.d/deb.sury.org-php.list ) -eq 1 ] ; then
    _debug "Adding Sury repo .list to APT"
    echo "deb [signed-by=/usr/share/keyrings/deb.sury.org-php.gpg] https://packages.sury.org/php/ $(lsb_release -sc) main" > /etc/apt/sources.list.d/deb.sury.org-php.list
    apt-get -qqq update
fi

mkdir -p $ETC_SYMPL/php/domain

# make a list of the avilable PHP versions
available_php_versions=$( apt search --names-only '^php[0-9]\.[0-9]$' 2>/dev/null | grep -o '^php[0-9]\.[0-9]' | sed -e 's|^php||' | sort --version-sort )

_debug "Available PHP Versions: $available_php_versions\n"

function _get_domains() {
    # returns the list of domains in $SRV which contain expected directories
    find $SRV/ -maxdepth 2 -mindepth 2 -type d \( -name 'config' -o -name 'public' -o -name 'mailboxes' \) ! -path './__disabled__*' | sed "s|^$SRV/||" | cut -f '1' -d '/' | sort -u
}

function _config_exists(){
    domain="$1"
    config="$2"
    if [ -f "$SRV/$domain/config/$config" ]; then
	    return 0
    else
	    return 1
    fi
}

function _config_get_version() {
    domain="$1"
    config="$2"
    if [ -f "$SRV/$domain/config/$config" ]; then
        sed 's|#.*||' "$SRV/$domain/config/$config" | grep -o "[0-9\.]*" | head -n 1 | awk '{ print $1 }'
    else
        echo -n ''
    fi
}

function _config_get_alnum() {
    domain="$1"
    config="$2"
    if [ -f "$SRV/$domain/config/$config" ]; then
        sed 's|#.*||' "$SRV/$domain/config/$config" | grep -o "[[:alnum:]]*" | head -n 1 | awk '{ print $1 }'
    else
        echo -n ''
    fi
}

function _config_get_string() {
    domain="$1"
    config="$2"
    if [ -f "$SRV/$domain/config/$config" ]; then
        sed 's|#.*||' "$SRV/$domain/config/$config" | grep -o "[[:alnum:]\._-]*" | head -n 1 | awk '{ print $1 }'
    else
        echo -n ''
    fi
}

function _config_get_list() {
    domain="$1"
    config="$2"
    if [ -f "$SRV/$domain/config/$config" ]; then
        sed 's|#.*||' "$SRV/$domain/config/$config" | grep -o "[[:alnum:]\._-]*" | awk '{ print $1 }' | tr '[:upper:]' '[:lower:]'
    else
        echo -n ''
    fi
}

function _test_phpfpm_config() {
    test_result=$( /usr/sbin/php-fpm$1 -t --fpm-config /etc/php/$1/fpm/php-fpm.conf 2>&1 )
    errorcode=$?
    if [ $errorcode -eq 0 ]; then
	    return 0
    else
	    echo "$test_result"
	    return $errorcode
    fi
}

function _fpm_up() {
    php_version=$1
    # add the FPM pool.d and disable the default
    # if the default pool.d directory exists, and theres no sympl-web-php.conf...
    if [ -d "/etc/php/$php_version/fpm/pool.d" ] && [ ! -f "/etc/php/$php_version/fpm/pool.d/sympl-web-php.conf" ]; then
        # if the default www.conf pool is configured
        if [ -f "/etc/php/$php_version/fpm/pool.d/www.conf" ]; then
            # disable it
            mv "/etc/php/$php_version/fpm/pool.d/www.conf" "/etc/php/$php_version/fpm/pool.d/www.conf_disabled"
        fi
        # set up the includes
        echo "include=$ETC_SYMPL/php/$php_version/pool.d/*.conf" > "/etc/php/$php_version/fpm/pool.d/sympl-web-php.conf"
    fi
    # enable if the service is disabled, then enable it
    if ! systemctl is-enabled --quiet php$php_version-fpm ; then
        systemctl enable --quiet php$php_version-fpm

        # if we have monit scripts directory
        if [ -d "$ETC_SYMPL/monit.d" ]; then
            monit_file="$ETC_SYMPL/monit.d/php$( echo $php_version | tr -d '.' )-fpm"
            if [ ! -f "$monit_file" ]; then
                # create the monit script and make it executable
                echo -e "#!/usr/bin/ruby\nrequire 'symbiosis/monitor/check'\n# Ensure php$php_version-fpm is running\nclass PHPFPM < Symbiosis::Monitor::Check\ndef initialize\n    super pid_file: '/var/run/php/php$php_version-fpm.pid',\n        init_script: '/etc/init.d/php$php_version-fpm',\n        unit_name: 'php$php_version-fpm',\n        process_name: 'php$php_version-fpm'\n    end\nend\n\nexit PHPFPM.new.do_check if \$PROGRAM_NAME == __FILE__" > $monit_file
                chmod +x $monit_file
            fi
        fi

    fi
    # if the service isnt active, start it
    if ! systemctl is-active --quiet php$php_version-fpm ; then
        systemctl start --quiet php$php_version-fpm
        # FIXME: wait for the service to report it's reloaded/loaded
        # FIXME:  for now, we pause 1 second to allow it to catch up
        sleep 1
    fi
}

function _fpm_reload() {
    php_version=$1
    # add the FPM pool.d and disable the default
    # if the default pool.d directory exists, and theres no sympl-web-php.conf...
    if [ -d "/etc/php/$php_version/fpm/pool.d" ] && [ ! -f "/etc/php/$php_version/fpm/pool.d/sympl-web-php.conf" ]; then
        # if the default www.conf pool is configured
        if [ -f "/etc/php/$php_version/fpm/pool.d/www.conf" ]; then
            # disable it
            mv "/etc/php/$php_version/fpm/pool.d/www.conf" "/etc/php/$php_version/fpm/pool.d/www.conf_disabled"
        fi
        # set up the includes
        echo "include=$ETC_SYMPL/php/$php_version/pool.d/*.conf" > "/etc/php/$php_version/fpm/pool.d/sympl-web-php.conf"
    fi
    # enable if the service is disabled
    if ! systemctl is-enabled --quiet php$php_version-fpm ; then
        systemctl enable --quiet php$php_version-fpm

        # if we have monit scripts directory
        if [ -d "$ETC_SYMPL/monit.d" ]; then
            monit_file="$ETC_SYMPL/monit.d/php$( echo $php_version | tr -d '.' )-fpm"
            if [ ! -f "$monit_file" ]; then
                # create the monit script and make it executable
                echo -e "#!/usr/bin/ruby\nrequire 'symbiosis/monitor/check'\n# Ensure php$php_version-fpm is running\nclass PHPFPM < Symbiosis::Monitor::Check\ndef initialize\n    super pid_file: '/var/run/php/php$php_version-fpm.pid',\n        init_script: '/etc/init.d/php$php_version-fpm',\n        unit_name: 'php$php_version-fpm',\n        process_name: 'php$php_version-fpm'\n    end\nend\n\nexit PHPFPM.new.do_check if \$PROGRAM_NAME == __FILE__" > $monit_file
                chmod +x $monit_file
            fi
        fi

    fi
    if systemctl is-active --quiet php$php_version-fpm ; then
        systemctl reload --quiet php$php_version-fpm
        # FIXME: wait for the service to report it's reloaded/loaded
        # FIXME:  for now, we pause 1 second to allow it to catch up
        sleep 1
    else
        systemctl start --quiet php$php_version-fpm
        # FIXME: wait for the service to report it's reloaded/loaded
        # FIXME:  for now, we pause 1 second to allow it to catch up
        sleep 1
    fi
}

function _fpm_down() {
    php_version=$1
    # add the FPM pool.d and disable the default
    # if the default pool.d directory exists, and theres is a sympl-web-php.conf...
    if [ -d "/etc/php/$php_version/fpm/pool.d" ] && [ -f "/etc/php/$php_version/fpm/pool.d/sympl-web-php.conf" ]; then
        # if the default www.conf pool is not configured and was moved...
        if [ ! -f "/etc/php/$php_version/fpm/pool.d/www.conf" ] && [ -f "/etc/php/$php_version/fpm/pool.d/www.conf_disabled" ]; then
            # put it back
            mv "/etc/php/$php_version/fpm/pool.d/www.conf_disabled" "/etc/php/$php_version/fpm/pool.d/www.conf"
        fi
        # remove the the sympl includes
	    rm -f "/etc/php/$php_version/fpm/pool.d/sympl-web-php.conf"
    fi
    # Remove the monit script so that doesn't bring it back up
    rm -f "$ETC_SYMPL/monit.d/php$( echo $php_version | tr -d '.' )-fpm"
    # bring the service down if up, then disable if enabled
    if systemctl is-active --quiet php$php_version-fpm ; then
        systemctl stop --quiet php$php_version-fpm
    fi
    if systemctl is-enabled --quiet php$php_version-fpm; then
        systemctl disable --quiet php$php_version-fpm
    fi
}


for domain in $( _get_domains ); do

    # does php-fpm exist?
    if [ -f "$SRV/$domain/config/php-fpm" ]; then
        # is the php-fpm file not-empty, disabling the option for apache?
        if [ -s "$SRV/$domain/config/php-fpm" ]; then
            # php-fpm is disabled, continue
            _echo "\nDomain: $domain\n"
            _echo "  PHP-FPM is disabled on as 'confg/php-fpm' contains text. Skipping.\n"
            continue
        else
            # php-fpm exists and is empty (normal)
            if [ ! -f "$SRV/$domain/config/php" ]; then
                # but there is no php file, so not going to be configured here, so disable it
                _echo "\nDomain: $domain\n"
                _echo "  PHP no longer configured for $domain. Falling back to defaults.\n"
		        rm -f "$SRV/$domain/config/php-fpm"
                continue
            fi
        fi
    elif [ ! -f "$SRV/$domain/config/php" ]; then
        # theres no php configuration for this site, continue silently
        continue
    fi

    # fallen through to normal operation - config/php exists and config/php-fpm is either empty or doesnt exist
    _echo "\nDomain: $domain\n"

    # remove the php-fpm flag for now, and we'll re-create it later when successful
    #   we've already checked it's not already disabled (ie: non-empty), but as
    #   changes are being made tot he config, if sympl-web-configure runs in the
    #   while this is running, we don't want to have a broken site
    if [ ! -f "$SRV/$domain/config/php-fpm" ]; then
        rm -f "$SRV/$domain/config/php-fpm"
    fi

    # get the PHP version we want from the config file
    php_version=$( _config_get_version $domain php )

    # check to see if the version requested is valid, and skip if it isn't, removing the php-fpm flag
    if [ $( echo "$available_php_versions" | grep -c "^$php_version$" ) -ne 1 ]; then
        if [ "x$php_version" == "x" ]; then
            _error "  No PHP version found in $SRV/$domain/config/php\n"
        else
            _error "  '$php_version' is not an available PHP version in $SRV/$domain/config/php\n"
            _error "  Available PHP versions are: $available_php_versions"
        fi
        rm -f "$SRV/$domain/config/php-fpm"
        continue
    fi

    _echo "  Configuring site for PHP $php_version\n"


    # List of php base packages to install
    php_install_list="php$php_version php$php_version-cli php$php_version-fpm php$php_version-common"

    php_packages_installed=$( dpkg -l "php*" | grep "^ii" | awk '{print $2}' )

    # Check if all the base php packages already installed, and install them if not
    for php_package in $php_install_list; do
        
        if [ $( echo "$php_packages_installed" | grep -c "^$php_package$") -eq 0 ]; then
            # $php_package is not installed, install it
            _echo "  Installing $php_package ...\n"
            DEBIAN_FRONTEND=noninteractive apt-get -qqq -y install $php_package > /dev/null || true
        fi
    
    done
  
	# ensure the directories we'll be using exist
	mkdir -p $ETC_SYMPL/php/$php_version/pool.d $ETC_SYMPL/php/$php_version/include.d

    # select the default modules to attempt to install...
    case $php_version in
        7.2|7.3|7.4)
            php_modules="bz2 curl gd imagick intl mbstring mysql opcache readline xml xmlrpc zip"
            ;;
        5.*|7.*)
            php_modules="bz2 curl gd imagick intl mbstring mcrypt mysql opcache readline xml xmlrpc zip"
            ;;
        *)
            php_modules="bz2 curl gd imagick intl mbstring mysql opcache readline xml zip"
            ;;
    esac

    available_php_modules=$( apt search --names-only "^php$php_version-" 2>/dev/null | grep "^php$php_version-.*/" | sed -e "s|^php$php_version-||" -e 's|/.*||' -e 's|-dbgsym$||' | sort -u | grep -v '^common$\|^fpm$\|^cgi$\|^cli$\|^dev$' )

    # read the list of modules wanted from config/php-modules
    wanted_modules="$( _config_get_list $domain php-modules )"

    # run through the list of wanted PHP modules, and add them to the list if they're valid
    for module in $wanted_modules; do
        # check the module is in then list of available modules
        if [ $( echo "$available_php_modules" | grep -c "^$module$" ) -eq 1 ]; then
            # add the module to the end of the list
            php_modules="$php_modules $module"
        else
            _error "Warning: requested PHP module '$module' doesn't appear to be valid."
        fi
    done

    php_packages=""
    # run through the list of PHP modules, and turn them into a list of packages
    for module in $php_modules ; do
        php_packages="$php_packages php$php_version-$module"
    done

    # run through the list of packages we have
    for package in $php_packages ; do
        # check to see if the package is installed already
        if [ $( dpkg -l "$package" 2>/dev/null | grep -c '^ii' ) -ne 1 ]; then
            # its not installed, try to install it
            _echo "  Installing new package: $package\n"
            DEBIAN_FRONTEND=noninteractive apt-get -qqq -y install $package 2>&1 >/dev/null || true
        fi
    done

    # now we start configuring the pool

    # FIXME: this isn't ideal, as we're effectively re-creating the pool config for each domain
    # FIXME: ideally we should gather a set of configs, de-dupe them and *then* create the pools
    # FIXME: as-is, the '-old' config we create is dealt with at the end of this loop, so little risk

    # get the user if set as php-user or public-user
    if [ -f "$SRV/$domain/config/public-user" ] || [ -f "$SRV/$domain/config/php-user" ]; then
	    # php-user overides public-user
        if [ -f "$SRV/$domain/config/php-user" ]; then
            username=$( _config_get_string $domain php-user )
        else
            username=$( _config_get_string $domain public-user )
        fi
        _debug "username='$username'\n"
        uid=$( getent passwd "$username" 2>/dev/null | cut -f 3 -d ':' )
        if [ "$uid" == "" ]; then
            _echo "  Warning: User '$username' specified in $SRV/$domain/config/php-user or public-user does not exist.\n"
            user='www-data'
        elif [ $uid -lt 1000 ] || [ "$username" == "sympl" ] ; then
            _echo "  Warning: User '$username' specified in $SRV/$domain/config/php-user is invalid - is a system user.\n"
            user='www-data'
        else
            # specified user is valid, get the username from the UID
            user=$( getent passwd $uid | cut -f 1 -d ':' )
        fi
    else
	    user='www-data'
    fi

    # get the group if set as php-group or public-group
    if [ -f "$SRV/$domain/config/public-group" ] || [ -f "$SRV/$domain/config/php-group" ]; then
        # php-group overides public-group
        if [ -f "$SRV/$domain/config/php-group" ]; then
            group=$( _config_get_string $domain php-group )
        else
            group=$( _config_get_string $domain public-group )
        fi
        gid=$( getent group "$group" 2>/dev/null | cut -f 3 -d ':' )
        if [ "$gid" == "" ] ; then
            _echo "  Warning: Group '$group' specified in $SRV/$domain/config/php-group or public-group does not exist.\n"
            group='www-data'
        elif [ $gid -lt 1000 ] || [ "$group" == "sympl" ] ; then
            _echo "  Warning: Group '$group' specified in $SRV/$domain/config/php-group or public-group is invalid - is a system group.\n"
            group='www-data'
        else
            # specified user is valid, get the username from the UID
            group=$( getent group $gid | cut -f 1 -d ':' )
        fi
    else
	group='www-data'
    fi

    # create the pool in $ETC_SYMPL/php/<version>/pool.d

    if [ "$user" != "www-data" ] || [ "$group" != "www-data" ]; then
        if [ "$user" != "$group" ] && [ "$group" != "www-data" ]; then
     	    prefix="${user}_${group}_"
        else
            prefix="${user}_"
        fi
    else
        prefix=""
    fi

    if [ -f "$SRV/$domain/config/php-pool" ]; then
        pool_name="${prefix}$( _config_get_alnum $domain php-pool )"
    else
        pool_name="${prefix}default"
    fi

    pool_file="$ETC_SYMPL/php/$php_version/pool.d/${pool_name}.conf"

    _debug "pool_name='$pool_name'"
    _debug "pool_file='$pool_file'"

    # check if theres already a config, if so, move it out of the way
    #   if there isn't create a blank one for comparison later
    if [ -f "$pool_file" ]; then
        mv "$pool_file" "$pool_file-old"
    else
        touch "$pool_file-old"
    fi

    pool_socket="$ETC_SYMPL/php/${php_version}/${pool_name}.sock"

    echo "[$pool_name]
; This file has been automaticlly geenrated by Sympl
; Any changes will be overwitten!
; See https://wiki.sympl.io for configuration options,
; or create $ETC_SYMPL/php/$php_version/include.d/${pool_name}.conf
; with any overrides.

; user & group to run PHP processes as
user = $user
group = $group

; user and group to run socket as
listen = $pool_socket
listen.owner = www-data
listen.group = www-data

; pool management
pm = dynamic
pm.max_children = 16
pm.start_servers = 4
pm.min_spare_servers = 2
pm.max_spare_servers = 4

; php defaults
php_value[expose_php] = 0
php_value[upload_max_filesize] = 128M
php_value[post_max_size] = 128M

" > $pool_file

    # make sure the generated config is okay
    if ! _test_phpfpm_config $php_version; then
	    _error "Error encountered setting up default configuration for $pool_name. Skipping.\n"
        rm -f "$pool_file"
	    continue
    fi

    _echo "  Configured with '$pool_name' pool\n"
    if [ "$user" != "www-data" ] || [ "$group" != "www-data" ]; then
        _echo "  Running PHP with user '$user', and group '$group'\n"
    fi

    if [ -f "$ETC_SYMPL/php/$php_version/include.d/${pool_name}.conf" ]; then
	# add the include to the config
	echo "include = $ETC_SYMPL/php/$php_version/include.d/$pool_name.conf" >> $pool_file
    	if ! _test_phpfpm_config $php_version; then
            # include induced an error, roll it back
            _error "  Error with include for php$php_version-fpm $pool_name, excluding from pool configuration"
            sed -i '/^include = /d' $pool_file
        fi
    fi

    # check if the symlink socket needs updating
    # if it exists, 
    if [ -L $ETC_SYMPL/php/domain/$domain ]; then
        # check if different from what we expect
        if [ "$( readlink "$ETC_SYMPL/php/domain/$domain" )" != "$pool_socket" ]; then
            _debug "readlink is '$( readlink $ETC_SYMPL/php/domain/$domain )'\nwanted '$pool_socket'\n"
            # link is wrong, remove the link and create a new one
            rm $ETC_SYMPL/php/domain/$domain
            ln -s $pool_socket $ETC_SYMPL/php/domain/$domain
            _echo "  Updated pool link for domain\n"
        fi
        # else it exists and is what we expect
    else
        # no link where we expect, add it
        ln -s $pool_socket $ETC_SYMPL/php/domain/$domain
        _echo "  Created pool link for domain\n"
    fi

    #TODO: check for any .htaccess files in the webroot, copy relevant lines to .user.ini in the correct format

    find $SRV/$domain/public/htdocs -name '.htaccess' -type f ! -empty -printf '%h\n' | while read path ; do
        # set some variabled for readability
        htaccess="${path}/.htaccess"
        userini="${path}/.user.ini"
        _debug "parsing $htacces\n"
        # check that the identified file contains 'php_value', not in a comment
        if [ $( sed -e 's|#.*||' $htaccess | grep -c '\s*php_value\s' ) -gt 0 ]; then
            # check is a .user.ini exists
            if [ -f "$userini" ] && [ $( grep -c "# This configuration is automatically generated by Sympl" $userini) -eq 0 ]; then
                # file exists and doesn't say it's managed by Sympl, skip
                continue
            fi
            
            # copy .htaccess to .user.ini-new, retaining permissions
            cp -p $htaccess $userini-new
            # empty the file
            echo -n '' > $userini-new

            # write the config info to $userini-new
            echo "# This configuration is automatically generated by Sympl."                         >  $userini-new
            echo "# The content of this file is taken from .htaccess and regenerated periodically."  >> $userini-new
            echo "#"                                                                                 >> $userini-new
            echo "# Ensure any changes or additons here are also made in .htaccess"                  >> $userini-new
            echo "#   or remove this header to manage it manually."                                  >> $userini-new
            echo                                                                                     >> $userini-new
            sed -e 's|#.*||' $htaccess | grep '\s*php_value\s' | awk '{ printf $2 " = " ; $1=$2="" ; print $3 }' >> $userini-new

            if [ -f "$userini" ]; then
                if ! diff "$userini" "$userini-new" > /dev/null ; then
                    # theres a change
                    _debug "replace $userini\n"
                    mv "$userini-new" "$userini"
                    _echo "  Updated $( echo "$userini" | sed "s|^$SRV/$domain/public/htdocs/||" ) with new .htaccess entries\n"
                else
                    # they're the same, leave it
                    _debug "unchanged $userini\n"
                    rm "$userini-new"
                fi
            else
                # file doesnt exist, create it
                _debug "new $userini\n"
                mv "$userini-new" "$userini"
                _echo "  Created $( echo "$userini" | sed "s|^$SRV/$domain/public/htdocs/||" ) with new .htaccess entries\n"
            fi
        fi
    done

    # everything is ready, set the PHP-FPM flag for the domain
    if [ ! -f "$SRV/$domain/config/php-fpm" ]; then
        touch "$SRV/$domain/config/php-fpm"
	    chown sympl:sympl "$SRV/$domain/config/php-fpm"
	    chmod 660 "$SRV/$domain/config/php-fpm"
        _echo "  PHP-FPM enabled on $domain\n"
    fi

    # check if the 'old' config and the newly generated one is different at all
        # if it's different, flag the php-fpm instance as needing a reload in $changed_conf
        # if it's the same, do nothing

    if ! diff "$pool_file" "$pool_file-old" > /dev/null ; then
        _echo "  Pool configuration changed, will reload PHP $php_version\n"
        changed_conf="${changed_conf}${php_version}\n"
        # remove the old config
        rm "$pool_file-old"
    else
        # move the 'old' config back over the newly created one to retain timestamps
        mv "$pool_file-old" "$pool_file"

    fi

    # add this pool to the list of active pools
    active_pools="$active_pools\n$php_version\t$pool_name"

done

# turn the list of pools into a sorted list, removing any duplicates
active_pools=$( echo -ne "$active_pools" | grep . | sort -u )
changed_conf=$( echo -ne "$changed_conf" | grep . | sort -u )

_echo '\n'

# no need to run this if we have *never* set up a pool for sympl
if [ $( find $ETC_SYMPL/php/ -maxdepth 2 -mindepth 2 -type d -name 'pool.d' | wc -l ) -gt 0 ]; then

    _echo "Applying configuration..."

    _debug "\nactive_pools='$active_pools'\n"
    _debug "\nchanged_conf='$changed_conf'\n"

    while read php_version ; do
        # if we have an empty string (ie: no php configs at all) then continue
        if [ "x$php_version" == "x" ]; then continue ; fi

        _echo '.'

        _debug "Cleaning up PHP $php_version\n"
        keep_pools=$( echo "$active_pools" | grep -P "^$php_version\t" | cut -f 2 )
        for active_pool in $( find $ETC_SYMPL/php/$php_version/pool.d -maxdepth 1 -mindepth 1 -name '*.conf' -printf '%P\n' | sed 's|.conf$||' ); do
            if [ $( echo "$keep_pools" | grep -c "^$active_pool$" ) -ne 1 ]; then
                rm $ETC_SYMPL/php/$php_version/pool.d/$active_pool.conf
            fi
        done

        pool_count=$( find /etc/php/$php_version/fpm/pool.d/ $ETC_SYMPL/php/$php_version/pool.d -type f -name '*.conf' ! -name 'sympl-web-php.conf' | wc -l )

        if [ $pool_count -eq 0 ] ; then
            # theres no sites using this pool, take it down
            _debug "down php$php_version-fpm\n"
            _fpm_down $php_version
        else
            # if the version we're looking at is in the list of configs we've changed, reload it, otherwise bring it up
            if [ $( echo "$changed_conf" | grep -c "^$php_version$" ) -eq 1 ]; then
                _debug "reload php$php_version-fpm\n"
                _fpm_reload $php_version
            else
                _debug "up php$php_version-fpm\n"
                _fpm_up $php_version
            fi
            
        fi

    done <<< $( find $ETC_SYMPL/php/*/pool.d -maxdepth 1 -mindepth 1 -name '*.conf' | cut -f 5 -d '/' | sort -u )

fi

# clean up $ETC_SYMPL/php/domain, removing dead links
#   handily, you can use 'find' to find entries in the filesystem, and then '-e' will tell you
#   if target exists, rather than if the symlink itself

_echo '.'

find $ETC_SYMPL/php/domain -maxdepth 1 -mindepth 1 -type l | while read symlink ; do
    if [ ! -e "$symlink" ] && [ -L "$symlink" ]; then rm -f "$link" ; fi
done

# Disable error trapping now we're done
trap - ERR

# everything should be ready, reconfigure apache to use it

sympl-web-configure || true

_echo ' done\n\n'

exit 0
